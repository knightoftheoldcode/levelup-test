/**
 * Module dependencies.
 */

process.env.NODE_ENV = 'test';

const app = require('../../app');
const debug = require('debug')('levelup-mean:testserver');
const http = require('http');

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  const localPort = parseInt(val, 10);

  if (isNaN(localPort)) {
    // named pipe
    return val;
  }

  if (localPort >= 0) {
    // port number
    return localPort;
  }

  return false;
}

/**
 * Get port from environment and store in Express.
 */

const port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

/**
 * Create HTTP server.
 */

const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

// server.listen(port);
// server.on('error', onError);
// server.on('listening', onListening);

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string'
    ? `Pipe ${port}`
    : `Port ${port}`;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  const addr = server.address();
  const bind = typeof addr === 'string'
    ? `pipe ${addr}`
    : `port ${addr.port}`;
  debug(`Listening on ${bind}`);
}


const start = function start(serverPort, callback) {
  server.listen(serverPort, callback);
  server.on('error', onError);
  server.on('listening', onListening);
};

const stop = function stop(callback) {
  server.close(callback);
};

module.exports = {
  start,
  stop,
};
